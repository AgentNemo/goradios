package goradios

import (
	json2 "encoding/json"
	"fmt"
	"strconv"
)

const (
	StatesURL = "https://de1.api.radio-browser.info/json/states/%s"
)

type State struct {
	Name         string `json:"name"`
	Country      string `json:"country"`
	StationCount int    `json:"stationcount"`
}

func FetchStates(country string) []State {
	res := Post(GenerateStatesURL(country), "", nil)
	return UnmarshalStates(res)
}

func FetchStatesDetailed(country string, order Order, reverse bool, hideBroken bool) []State {
	q := make(map[string]string)
	q["order"] = string(order)
	q["reverse"] = strconv.FormatBool(reverse)
	q["hidebroken"] = strconv.FormatBool(hideBroken)
	res := Post(GenerateStatesURL(country), "", q)
	return UnmarshalStates(res)
}

func UnmarshalStates(json string) []State {
	var states []State
	json2.Unmarshal([]byte(json), &states)
	return states
}

func GenerateStatesURL(country string) string {
	if len(country) == 0 {
		return fmt.Sprintf(StatesURL, "")
	}
	return fmt.Sprintf(StatesURL, country)
}
