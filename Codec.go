package goradios

import (
	json2 "encoding/json"
	"strconv"
)

const (
	CodecsURL = "https://de1.api.radio-browser.info/json/codecs"
)

type Codec struct {
	Name         string `json:"name"`
	StationCount int    `json:"stationcount"`
}

func FetchCodecs() []Codec {
	res := Post(CodecsURL, "", nil)
	return UnmarshalCodecs(res)
}

func FetchCodecsDetailed(order Order, reverse bool, hideBroken bool) []Codec {
	q := make(map[string]string)
	q["order"] = string(order)
	q["reverse"] = strconv.FormatBool(reverse)
	q["hidebroken"] = strconv.FormatBool(hideBroken)
	res := Post(CountriesCodeURL, "", q)
	return UnmarshalCodecs(res)
}

func UnmarshalCodecs(json string) []Codec {
	var codecs []Codec
	json2.Unmarshal([]byte(json), &codecs)
	return codecs
}
