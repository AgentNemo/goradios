package goradios

import (
	"io/ioutil"
	"net/http"
	"strings"
)

func Post(url string, payload string, query map[string]string) string {

	payloadReader := strings.NewReader(payload)
	req, _ := http.NewRequest("GET", url, payloadReader)

	q := req.URL.Query()
	for key, value := range query {
		q.Add(key, value)
	}
	req.URL.RawQuery = q.Encode()
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	return string(body)
}
