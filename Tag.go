package goradios

import (
	json2 "encoding/json"
	"strconv"
)

const (
	TagsURL = "https://de1.api.radio-browser.info/json/tags"
)

type Tag struct {
	Name         string `json:"name"`
	StationCount int    `json:"stationcount"`
}

func FetchTags() []Tag {
	res := Post(TagsURL, "", nil)
	return UnmarshalTags(res)
}

func FetchTagsDetailed(order Order, reverse bool, hideBroken bool) []Tag {
	q := make(map[string]string)
	q["order"] = string(order)
	q["reverse"] = strconv.FormatBool(reverse)
	q["hidebroken"] = strconv.FormatBool(hideBroken)
	res := Post(TagsURL, "", q)
	return UnmarshalTags(res)
}

func UnmarshalTags(json string) []Tag {
	var languages []Tag
	json2.Unmarshal([]byte(json), &languages)
	return languages
}
