package goradios

import (
	json2 "encoding/json"
	"strconv"
)

const (
	LanguagesURL = "https://de1.api.radio-browser.info/json/languages"
)

type Language struct {
	Name         string `json:"name"`
	StationCount int    `json:"stationcount"`
}

func FetchLanguages() []Language {
	res := Post(LanguagesURL, "", nil)
	return UnmarshalLanguages(res)
}

func FetchLanguagesDetailed(order Order, reverse bool, hideBroken bool) []Language {
	q := make(map[string]string)
	q["order"] = string(order)
	q["reverse"] = strconv.FormatBool(reverse)
	q["hidebroken"] = strconv.FormatBool(hideBroken)
	res := Post(LanguagesURL, "", q)
	return UnmarshalLanguages(res)
}

func UnmarshalLanguages(json string) []Language {
	var languages []Language
	json2.Unmarshal([]byte(json), &languages)
	return languages
}
